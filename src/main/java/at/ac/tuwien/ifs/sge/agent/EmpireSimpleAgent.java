package at.ac.tuwien.ifs.sge.agent;

import java.util.*;

import at.ac.tuwien.ifs.sge.core.agent.AbstractRealTimeGameAgent;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.game.GameConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.EmpireEvent;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.*;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitAppearedActionResult;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitHitActionResult;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitMoveActionResult;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.action.result.UnitVanishedActionResult;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.CombatStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.MovementStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.communication.event.order.start.ProductionStartOrder;
import at.ac.tuwien.ifs.sge.game.empire.core.Empire;
import at.ac.tuwien.ifs.sge.game.empire.exception.EmpireMapException;
import at.ac.tuwien.ifs.sge.game.empire.map.Position;
import at.ac.tuwien.ifs.sge.game.empire.model.configuration.EmpireConfiguration;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireCity;
import at.ac.tuwien.ifs.sge.game.empire.model.map.EmpireProductionState;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnit;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitState;
import at.ac.tuwien.ifs.sge.game.empire.model.units.EmpireUnitType;

/*
    Empire Simple Agent

    A simple agent that only reacts to updates and tries to keep all units busy, while producing as many units as possible.

    Employs all units based on the following principle:
    produce new unit > engage in combat > move to unoccupied city > move to enemy > explore map
 */
public class EmpireSimpleAgent extends AbstractRealTimeGameAgent<Empire, EmpireEvent> {

    /*
        The agent is started as a new process that gets passed three arguments:
            1. the player id of the player it is going to represent
            2. the player name of the player it is going to represent
            3. the game class name of the game it should play. This is only relevant if the agent supports multiple games.

        Since the 'Main-Class' attribute of the JAR file defined in the build.gradle points to this class
        a main method is defined which gets called when the engine starts the agent. The agent is then instantiated
        and started through the start() method, which also starts the agent's communication with the engine server.
    */
    @SuppressWarnings("unchecked")
    public static void main(String[] args) {
        var playerId = getPlayerIdFromArgs(args);
        var playerName = getPlayerNameFromArgs(args);
        var agent = new EmpireSimpleAgent(playerId, playerName, 0);
        agent.start();
    }

    private static int MAX_UNITS = 100;

    private final Random random = new Random();

    private Map<UUID, EmpireUnitState> unitState = new HashMap<>();
    private Map<Position, EmpireProductionState> cityState = new HashMap<>();
    private Map<Position, UUID> cityUnits = new HashMap<>();

    private List<EmpireUnitType> unitTypes;

    private int nrOfUnits = 0;

    /*
        A constructor calling the super constructor is required when extending the abstract agent.

        Since this agent only plays Empire the game class can be passed as shown.
     */
    public EmpireSimpleAgent(int playerId, String playerName, int logLevel) {
        super(Empire.class, playerId, playerName, logLevel);
    }

    /*
        When the abstract agent receives an update from the server it is applied to the agents game after which
        the method onGameUpdate(A action, ActionResult result) is called. The parameters represent the
        action that has been applied as well as the result it created. The updated game state can be found
        in the protected field 'game' of the abstract agent implementation.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The EmpireSimpleAgent only reacts to actions which is not computationally heavy and can therefore be
        done directly in the 'onGameUpdate' method.
     */
    @Override
    protected void onGameUpdate(HashMap<EmpireEvent, ActionResult> actionsWithResult) {
        for (var entry : actionsWithResult.entrySet()) {
            handleActionFromGameUpdate(entry.getKey(), entry.getValue());
        }
    }

    private void handleActionFromGameUpdate(EmpireEvent action, ActionResult result) {
        // Employs all units that do nothing after the applied action from the update
        if (action instanceof ProductionAction pa) {
            // production actions happen when aunit is produced
            var city = getGame().getCity(pa.getCityPosition());
            if (city.getPlayerId() == playerId) {
                cityState.put(pa.getCityPosition(), EmpireProductionState.Idle);
                var producingUnitId = cityUnits.get(pa.getCityPosition());
                var producingUnit = producingUnitId != null ? getGame().getUnit(producingUnitId) : null;
                var newUnit = getGame().getUnit(pa.getUnitId());
                unitState.put(pa.getUnitId(), EmpireUnitState.Idle);
                if (producingUnit != null)
                    employUnit(producingUnit);
                employUnit(newUnit);
            }

        } else if (action instanceof MovementAction ma) {
            // movement actions happen a unit moves and the movement origin as well as destination is visible
            var unit = getGame().getUnit(ma.getUnitId());
            var movementResult = (UnitMoveActionResult) result;
            if (unit.getPlayerId() == playerId && !movementResult.hasStartedDefending()) {
                unitState.put(ma.getUnitId(), EmpireUnitState.Idle);
                employUnit(unit);
            }
            employIfPlayersUnit(movementResult.getInterruptedMovingUnits());
            employIfPlayersUnit(movementResult.getInterruptedFightingUnits());

        } else if (action instanceof CombatHitAction cha) {
            // combat hit actions happen when both parties of a combat are visible
            var attacker = getGame().getUnit(cha.getAttackerId());
            var hitResult = (UnitHitActionResult) result;
            var killedUnit = hitResult.getKilledUnit();
            if (attacker.getPlayerId() == playerId) {
                if (killedUnit != null) {
                    employIfPlayersUnit(hitResult.getIdleUnits());
                }
            } else if (killedUnit != null && killedUnit.getPlayerId() == playerId) {
                handleOwnUnitKilled(killedUnit.getId());
            }

        } else if (action instanceof UnitDamagedAction uda) {
            // unit damaged actions happen when the damaged unit's attacker is not visible
            var targetId = uda.getTargetId();
            var hitResult = (UnitHitActionResult) result;
            var killedUnit = hitResult.getKilledUnit();
            if (killedUnit != null) {
                if (killedUnit.getPlayerId() == playerId)
                    handleOwnUnitKilled(targetId);
                else
                    employIfPlayersUnit(hitResult.getIdleUnits());
            }
        } else if (action instanceof UnitVanishedAction uva) {
            // unit vanished actions happen when vision on a unit is lost
            var unitVanishedResult = (UnitVanishedActionResult) result;
            employIfPlayersUnit(unitVanishedResult.getInterruptedFightingUnits());
        } else if (action instanceof UnitAppearedAction uaa) {
            // unit appeared actions happen when an enemy unit moves into vision
            var unitAppearedResult = (UnitAppearedActionResult) result;
            employIfPlayersUnit(unitAppearedResult.getStoppedMovingUnits());
        }
    }

    // employs units if they are idle and belong to the agent
    private void employIfPlayersUnit(List<UUID> unitIds) {
        unitIds.forEach(unitId -> {
            var unit = getGame().getUnit(unitId);
            if (unit.getPlayerId() == playerId) {
                unitState.put(unitId, EmpireUnitState.Idle);
                employUnit(unit);
            }
        });
    }

    // handles the death of a unit by starting unit production
    private void handleOwnUnitKilled(UUID unitId) {
        nrOfUnits--;
        unitState.remove(unitId);
        Position cityUnitPosToRemove = null;
        for (var entry : cityUnits.entrySet()) {
            var cityUnitId = entry.getValue();
            var cityUnit = getGame().getUnit(cityUnitId);
            if (cityUnit == null)
                cityUnitPosToRemove = entry.getKey();
            else
                employUnit(cityUnit);
        }
        if (cityUnitPosToRemove != null)
            cityUnits.remove(cityUnitPosToRemove);
    }


    /*
        In case of an action that was sent to the server and is not valid or no longer valid, the server responds
        with an InvalidActionEvent which contains the action that has been rejected. The abstract agent then
        calls the onActionRejected(A action) method.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The EmpireSimpleAgent only reacts to rejected actions which is not computationally heavy and can therefore be
        done directly in the 'onActionRejected' method.
     */
    @Override
    protected void onActionRejected(EmpireEvent action) {
        // Re-employs the unit that was associated with the rejected action
        if (action instanceof CombatStartOrder cso) {
            var unitId = cso.getAttackerId();
            unitState.put(unitId, EmpireUnitState.Idle);
            employUnit(getGame().getUnit(unitId));

        } else if (action instanceof MovementStartOrder mso) {
            var unitId = mso.getUnitId();
            unitState.put(unitId, EmpireUnitState.Idle);
            employUnit(getGame().getUnit(unitId));

        } else if (action instanceof ProductionStartOrder pso) {
            nrOfUnits--;
            cityState.put(pso.getCityPosition(), EmpireProductionState.Idle);
            var producingUnit = getGame().getUnit(cityUnits.get(pso.getCityPosition()));
            if (producingUnit != null)
                employUnit(producingUnit);
        }
    }

    /*
         After all agents have been started by the engine and the game has been initialized with the starting configuration
         all agents receive their game configuration (consisting only of the information they are allowed to have) and the
         number of players. Afterwards the setup(GameConfiguration gameConfiguration, int numberOfPlayers) method is called.
         When overriding the 'setup' method ALWAYS call the super method, to ensure that the game is instantiated with the
         right parameters and all fields are setup correctly.

         The unit types from the used configuration are stored in a seperate field.
    */
    @Override
    public void setup(GameConfiguration gameConfiguration, int numberOfPlayers) {
        super.setup(gameConfiguration, numberOfPlayers);
        this.unitTypes = ((EmpireConfiguration) gameConfiguration).getUnitTypes();
    }

    /*
        After every agent is setup, the engine server issues a StartGameEvent which indicates that it is now
        ready to receive and process actions from the agents.

        DO NOT RUN EXTENSIVE CODE IN THIS METHOD as it will block the communication with the server!
        Instead use another thread.

        The EmpireSimpleAgent simply employs all starting units at the beginning.
     */
    @Override
    public void startPlaying() {
        var units = getGame().getUnitsByPlayer(playerId);
        for (var unit : units) {
            unitState.put(unit.getId(), EmpireUnitState.Idle);
            nrOfUnits++;
        }
        for (var unit: units)
            employUnit(unit);
    }

    /*
        After the game is over the agent receives a TearDownEvent event, which signals it to stop
        the computation and shut down gracefully.

        Since the agent only reacts there is nothing to stop or shut down.
    */
    @Override
    public void shutdown() {}

    // determines the best action for the given unit and sends it to the server.
    private void employUnit(EmpireUnit unit) {
        if (unit == null) return;

        // Unit states are stored locally since state changes are not applied directly after sending an action but
        // only after the action was processed by the server and the update was received and applied by the agent
        if (unitState.getOrDefault(unit.getId(), EmpireUnitState.Idle) != EmpireUnitState.Idle) return;

        var pos = unit.getPosition();
        var cities = getGame().getCitiesByPosition();
        var map = getGame().getBoard();

        // If there is a city at the units position and there is not already a unit that holds this city then start
        // defending it. When neither the unit cap nor MAX_UNITS is reached and the city is not producing start producing
        if (cities.containsKey(pos)) {
            var cityUnit = cityUnits.getOrDefault(pos, null);
            if (cityUnit == null) {
                cityUnits.put(pos, unit.getId());
                unitState.put(unit.getId(), EmpireUnitState.Idle);
                cityUnit = unit.getId();
            }
            if (unit.getId() == cityUnit) {
                if (cityState.getOrDefault(pos, EmpireProductionState.Idle) == EmpireProductionState.Idle) {
                    if (nrOfUnits < MAX_UNITS && nrOfUnits < getGame().getGameConfiguration().getUnitCap()) {
                        var unitType = unitTypes.get(random.nextInt(unitTypes.size()));
                        var action = new ProductionStartOrder(pos, unitType.getUnitTypeId());
                        sendAction(action, System.currentTimeMillis() + 50);
                        cityState.put(pos, EmpireProductionState.Producing);
                        nrOfUnits++;
                    }
                }
                return;
            }
        }

        try {
            var actions = map.getPossibleActions(unit);

            // Separate actions into movement and combat
            var combatActions = new ArrayList<CombatStartOrder>();
            var moveActions = new ArrayList<MovementStartOrder>();
            for (var action : actions) {
                if (action instanceof  CombatStartOrder cso) combatActions.add(cso);
                else  if (action instanceof MovementStartOrder mso) moveActions.add(mso);
            }

            // Find the best action according to this principle: possible combat > move to unoccupied city > move to enemy > explore map
            EmpireEvent bestAction = null;
            if (combatActions.size() != 0) {
                bestAction = combatActions.get(random.nextInt(combatActions.size()));
            } else if (moveActions.size() != 0) {
                var actionToCity = getMovementOrderToCityTile(moveActions);
                if (actionToCity == null) {
                    var nearestEnemy = findNearestEnemyUnit(unit);
                    if (nearestEnemy == null) {
                        bestAction = getMovementOrderWithMostExploration(moveActions, unit);
                    } else {
                        bestAction = getBestMovementOrderToDestination(moveActions, nearestEnemy.getPosition());
                    }
                }
                else bestAction = actionToCity;
            }

            // send the best action and also set the local unit state to prevent sending multiple actions per unit which state has not yet updated
            if (bestAction != null) {
                sendAction(bestAction, System.currentTimeMillis() + 50);
                unitState.put(unit.getId(), combatActions.size() == 0 ? EmpireUnitState.Moving : EmpireUnitState.Fighting);
            }

        } catch (EmpireMapException e) {
            log.error("Could not get actions for unit with id " + unit.getId());
            log.printStackTrace(e);
        }
    }

    // Get a MovementStartOrder to a city tile if possible
    private MovementStartOrder getMovementOrderToCityTile(List<MovementStartOrder> movementActions) throws EmpireMapException {
        for (var action : movementActions) {
            var tile = getGame().getBoard().getTile(action.getDestination());
            if (tile instanceof EmpireCity && tile.getOccupants().size() == 0) {
                return action;
            }
        }
        return null;
    }

    // Get the MovementStartOrder that results in the largest exploration of the map
    private MovementStartOrder getMovementOrderWithMostExploration(List<MovementStartOrder> movementActions, EmpireUnit unit) {
        var fov = unit.getFov();
        var map = getGame().getBoard();
        var discoveredByPosition = map.getDiscoveredByPosition();
        var visionByPosition = map.getActiveVisionByPosition();

        var bestNrOfUndiscoveredTiles = 0;
        var bestNrOfHiddenTiles = 0;
        var bestAction = movementActions.get(0);

        for (var action : movementActions) {
            var pos = action.getDestination();
            var nrOfUndiscoveredTiles = 0;
            var nrOfHiddenTiles = 0;
            for (var y = pos.getY() - fov; y <= pos.getY() + fov; y++) {
                for (var x = pos.getX() - fov; x <= pos.getX() + fov; x++) {
                    if (!map.isInside(x,y)) continue;
                    var tilePos = new Position(x, y);
                    var hasCurrentlyVision = visionByPosition.get(tilePos)[playerId].hasVision();
                    var hasDiscoveredOnce = discoveredByPosition.get(tilePos)[playerId];
                    if (!hasCurrentlyVision) {
                        if (hasDiscoveredOnce)
                            nrOfHiddenTiles++;
                        else
                            nrOfUndiscoveredTiles++;
                    }

                }
            }
            if (nrOfUndiscoveredTiles >= bestNrOfUndiscoveredTiles) {
                if (nrOfUndiscoveredTiles == bestNrOfUndiscoveredTiles) {
                    if (nrOfHiddenTiles > bestNrOfHiddenTiles) {
                        bestAction = action;
                        bestNrOfHiddenTiles = nrOfHiddenTiles;
                    }
                } else {
                    bestAction = action;
                    bestNrOfUndiscoveredTiles = nrOfUndiscoveredTiles;
                }
            }
        }
        return bestAction;
    }

    // Get the MovementStartOrder that brings the unit closest to destination
    private MovementStartOrder getBestMovementOrderToDestination(List<MovementStartOrder> movementActions, Position destination) {
        var bestDistance = Double.MAX_VALUE;
        var bestAction = movementActions.get(0);
        for (var action : movementActions) {
            var movementDestination = action.getDestination();
            var distance = getDistance(movementDestination, destination);
            if (distance < bestDistance) {
                bestDistance = distance;
                bestAction = action;
            }
        }
        return bestAction;
    }

    // Finds the nearest enemy unit
    private EmpireUnit findNearestEnemyUnit(EmpireUnit unit) {
        var unitPos = unit.getPosition();
        var enemyUnits = new ArrayList<EmpireUnit>();
        for (var i = 0; i < getGame().getNumberOfPlayers(); i++) {
            if (i != playerId)
                enemyUnits.addAll(getGame().getUnitsByPlayer(i));
        }
        var nearestDistance = Double.MAX_VALUE;
        EmpireUnit nearestUnit = null;
        for (var enemyUnit : enemyUnits) {
            var enemyPos = enemyUnit.getPosition();
            var distance = getDistance(unitPos, enemyPos);
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestUnit = enemyUnit;
            }
        }
        return nearestUnit;
    }

    private double getDistance(Position posA, Position posB) {
        var dx = posA.getX() - posB.getX();
        var dy = posA.getY() - posB.getY();
        return Math.sqrt(dx * dx + dy * dy);
    }

}
