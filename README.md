## Empire Simple Agent

A simple agent that only reacts to updates and tries to keep all units busy, while producing as many units as possible.

Employs all units based on the following principle:

1. Produce new unit 
2. Engage in combat 
3. Move to unoccupied city 
4. Move to enemy 
5. Explore map
